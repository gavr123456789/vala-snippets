//vala Files.vala --pkg gee-0.8 --pkg gio-2.0
using Gee;
//get file path, return Gee.ArrayList of all files in the directory recursively 
ArrayList<FileInfo> get_files(string path = ".") {
    var dir = File.new_for_path(path);
    var enum = dir.enumerate_children(FileAttribute.STANDARD_NAME, 0);
    FileInfo fi;
    var files = new ArrayList<FileInfo>();
    while((fi = enum.next_file()) != null) {
        if(FileUtils.test(path + "/" + fi.get_name(), FileTest.IS_DIR))
            files.add_all(get_files(fi.get_name()));
        else
            files.add(fi);
	}
    return files;
}
//get file path, return Gee.ArrayList of all dirs in the directory 
ArrayList<FileInfo> get_dirs(string path) {
	//var log = FileStream.open(path + "_filedirs_log.txt", "w");
	var dir = File.new_for_path(path);
	var enum = dir.enumerate_children(FileAttribute.STANDARD_NAME, 0);
	FileInfo fi;
	var dirs = new ArrayList<FileInfo>();
	while((fi = enum.next_file()) != null) {
		if(FileUtils.test(path + "/" + fi.get_name(), FileTest.IS_DIR)){
			//log.puts(fi.get_name() + "\n");
			dirs.add(fi);
		}
	}
	return dirs;
}

void rename_dirs(string path, string[] rename_names) {
	var dirs = get_dirs(path);
    if(dirs.size > rename_names.length) 
		message(@"The number of names passedis less than the number of folders.
    	Only the first $(rename_names.length) will be renamed.");
	uint i = 0;
	for (int a = 0; a < rename_names.length; a++) {
		if(dirs[a].get_name() != rename_names[a]){
			var d = File.new_for_path(path + "/" + dirs[a].get_name());
			d.set_display_name(rename_names[a]);
		}
	}
}

int rnd(int a = 0, int b = 10){
	return Random.int_range(a,b);
}

static int main(string[] args) {
	//rename dirs
	var (a, b, c) = new int[] { rnd(), rnd(), rnd()};
	rename_dirs(".", {@"$a",@"$b",@"$c"});
	//get all dirictoris recursively
	foreach (var file in get_files()) 
		stdout.printf (file.get_name() + "\n");
	return 0;
}

//Example
//    rename_dirs(".", {"1","2","3"});
//    foreach (var file in get_files()) 
//	  	  print(file.get_name() + "\n");

