public abstract class Animal : Object {
   public string food { get; set; default = "grass"; }

   public void eat() {
       stdout.printf(@"*chomp chomp* $food\n");
   }

   public abstract void say_hello();
}

public class Tiger : Animal {

   public Tiger(){food = "meat";}
   public override void say_hello() {
       stdout.printf("*roar*\n");
   }
}

public class Duck : Animal {

   public override void say_hello() {
       stdout.printf("*quack*\n");
   }
   
}

void main(){

   var tiger = new Tiger();
   var duck = new Duck();

   tiger.say_hello();
   duck.say_hello();

   duck.eat();
   tiger.eat();
   
}