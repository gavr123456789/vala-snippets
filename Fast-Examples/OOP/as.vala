class Foo : Object {
  public void my_method() {
    print("Foo\n");
   }
}

class Bar : Foo {
  public new void my_method() {
    print("Bar\n");
   }
}

void main() {
  var bar = new Bar();
  bar.my_method();
  (bar as Foo).my_method();
}