int thread_func() {
	print("Thread running.\n");
	Thread.usleep (1000000);
    return 42;
}

int main() {

	var t = new Thread<int>("my frst thread",thread_func);
	int result = t.join ();
	print(@"$result");

    return 0;
}