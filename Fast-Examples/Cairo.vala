using Gtk;
using Cairo;

public class MyWidget : Gtk.DrawingArea {
    double x;
    double y;
    double x1;
    double y1;
    bool frst;
    Context cr;

    public bool draw_line(){
        cr.set_line_join (LineJoin.ROUND);
        cr.set_source_rgb (1, 0, 0);
        cr.set_line_width (1);
        if(frst){
            cr.line_to (x, y);   //print(@"1 $x $y\n");
            cr.line_to (x1, y1); //print(@"2 $x1 $y1\n");
            cr.stroke (); print("SUS\n");
        }
        return false;
    }
    public MyWidget () {
        x=0;y=0;
        this.set_events (Gdk.EventMask.ENTER_NOTIFY_MASK |
                         Gdk.EventMask.BUTTON_PRESS_MASK | 
                         Gdk.EventMask.POINTER_MOTION_MASK);

        this.enter_notify_event.connect ((event) => {
            stdout.printf ("mouse entered !!! \n");
            return false;
        });

        this.button_press_event.connect ((e) => {
            stdout.printf(@"mouse $(e.button) click \n");
            
            if(!frst){
                x=e.x; 
                y=e.y; 
                frst = true;
                
            } else{
                if (e.button == 1){
                    x = x1;
                    y = y1;
                }

                x1 = e.x;
                y1 = e.y;
                
                draw_line();
                queue_draw();// по сути redraw
            }
            
            
            return false;
        });
            
        this.motion_notify_event.connect((event) => { 
            return false; 
        });

        this.draw.connect ((c) => {
            if (frst){
                c.set_operator (Cairo.Operator.OVER); //накладывание
                c.set_source_surface (cr.get_target (), 0, 0);// поверхности на саму себя
                c.paint();
            }
            this.cr = c;
            return false;
        });
    }
}

public void main (string[] args) {
    Gtk.init (ref args);
    var window = new Gtk.Window ();
    window.set_default_size (450, 550);
    window.add (new MyWidget ());
    window.destroy.connect (Gtk.main_quit); 
    window.show_all ();
    Gtk.main ();
}

