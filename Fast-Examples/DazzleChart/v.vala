// valac --pkg gtk+-3.0 --pkg libdazzle-1.0 v.vala
public static int main (string[] argv) {
    Gtk.init (ref argv);
    var win = new Gtk.Window ();
    var gv = new Dazzle.GraphView ();
    var gm = new Dazzle.GraphModel ();
    var b = new Gtk.Button.with_label ("add datapoint");
    var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 10);
    var gc = new Dazzle.GraphColumn ("The Value", Type.from_name ("gdouble"));
    var gr = new Dazzle.GraphLineRenderer ();
    var l = new Gtk.Label ("This is Dazzle Chart");
 
    gm.set_timespan (TimeSpan.SECOND); // Change this to change the speed, MINUTE,HOUR,DAY,MILLISECOND,SECOND
    gm.set_max_samples (30);
    gm.add_column (gc);
 
    b.clicked.connect (() => {
        Dazzle.GraphModelIter gi;
        gm.push (out gi, GLib.get_monotonic_time ());
        var rand = Random.double_range (0, 100);
        Dazzle.GraphModel.iter_set_value (gi, 0, rand);
        message (@"added $rand");
    });
    gm.changed.connect (() => { message ("changed signal"); });
    box.expand = true;
    gv.expand = true;
    box.pack_start (b, false, false, 0);
    Gdk.RGBA linecol = Gdk.RGBA ();
    linecol.red = 1.0; linecol.green = 0.0; linecol.blue = 0.0; linecol.alpha = 1.0;
    gr.stroke_color_rgba = linecol;
    gr.line_width = 1;
    gr.column = 0;
    gv.add_renderer (gr);
    box.pack_start (gv, true, true, 0);
    box.pack_start (l, false, false, 0);
    gv.set_model (gm);
    win.add (box);
    win.show_all ();
    win.delete_event.connect (() => { Gtk.main_quit (); return true; });
    Gtk.main ();
    return 0;
}