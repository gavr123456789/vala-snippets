//https://gitlab.com/kosmospredanie/gpseq
using Gpseq;

void main () {
    string[] array = {"dog", "cat", "pig", "boar", "bear"};
    Seq.of_array<string>(array)
        .parallel()
        .filter(g => g.length == 3)
        .map<string>(g => g.up())
        .foreach(g => print("%s\n", g))
        .wait();
}

// (possibly unordered) output:
// DOG
// CAT
// PIG

