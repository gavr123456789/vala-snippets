using Gee;

class SmplWrapper<T> {
  public T val;
}

public static void main () {
  print("hj");

  var list = new ArrayList<int>.wrap ({1,2,3});print("2");
  var list_wrap = new SmplWrapper<Gee.List<int> > ();
  list_wrap.val = list;print("4");

  var int_wrap = new SmplWrapper<int?>();
  var str_wrap = new SmplWrapper<string?>();
  int_wrap.val = 5;
  str_wrap.val = "sas";


  print(@"int : $(int_wrap.val)
str : $(str_wrap.val)
list: ");
  list_wrap.val.@foreach( (a) => {print(@"$a, "); return true;} );
}