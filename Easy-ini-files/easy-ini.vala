[SingleInstance]
class Setting : Object {
	private  KeyFile file;
	private string _group = "base";
	private string filename = "settings.conf";
	private string path = "";

	public void open(string filename = "settings.conf"){
		if (!File.new_for_path (path + filename).query_exists ()){
			
		}
		file = new KeyFile ();
		//file.set_list_separator (',');
		file.load_from_file (filename, KeyFileFlags.NONE);
	}
	public  void set (string key,string val){
		file.set_value(group, key, val);
	}
	public  string get (string key){
		return file.get_string(group, key);
	}
	public  void save(){
		file.save_to_file("settings.conf");
	}
	public string group {
		set { _group = value; }
		get { return _group; }
	}
}

static int main(string[] args) {
    var sett = new Setting();
	sett.open(); // default filename = settings.conf
    sett["key"] = "value"; // ye, just like hashmap 
	var name = sett["name"];
    sett.save(); // save to file

	return 0;
}
//Example
//    var sett = new Setting();
//	  sett.open(); // default filename = settings.conf
//    sett["key"] = "value"; // ye, just like hashmap 
//	  var name = sett["name"];
//    sett.save(); // save to file