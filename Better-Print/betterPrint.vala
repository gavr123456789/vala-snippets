using Gee;
void prin(Variant str)  { stdout.printf(variant_to_string(str) + "\n");}
void prinobj(Value str) { stdout.printf(value_to_string(str) + "\n");}

string value_to_string (Value val){
      //var typeName = val.type_name(); message(typeName);
    string res = "";
    
    if (val.type().is_a (typeof (Gee.Traversable))) {
        if ( (val as Gee.Traversable).element_type == typeof (int)) res = ai_deserialization    ((val as Collection<int>).to_array());    else
        if ( (val as Gee.Traversable).element_type == typeof (string)) res = as_deserialization ((val as Collection<string>).to_array()); else        
        if ( (val as Gee.Traversable).element_type == typeof (bool)) res = ab_deserialization   ((val as Collection<bool>).to_array());   else        
        if ( (val as Gee.Traversable).element_type == typeof (double)) res = ad_deserialization ((val as Collection<double>).to_array());         
        //res = ai_deserialization ((val as Gee.Traversable.element_type).to_array());             message(res);
    } else
	if (val.type().is_a (typeof (Object)))    {res = to_json_string(val);}    else

	if (val.type().is_a (typeof (int)))    {res = @"$((int)val)";}    else
    if (val.type().is_a (typeof (string))) {res = (string)val;}       else
    if (val.type().is_a (typeof (bool)))   {res = @"$((bool)val)";}   else
    if (val.type().is_a (typeof (double))) {res = @"$((double)val)";} else
    if (val.type().is_a (typeof (string[]))) {
        string[] arr = (string[])val;
        res = as_deserialization(arr);
    } 

	//  if (val.type().is_a (typeof (ArrayList<int>))) {
    //      res = ai_deserialization ((val as ArrayList<int>).to_array());             message(res);
    //  } else  
    //  if (val.type().is_a (typeof (ArrayList<string>))) {
    //      res = as_deserialization ((val as ArrayList<string>).to_array());          message(res);
    //  } else
    //  //  if (val.type().is_a (typeof (ArrayList<bool>))) {
    //  //      res = ab_deserialization ((val as ArrayList<bool>).to_array());        message(res);
    //  //  } else
    //  if (val.type().is_a (typeof (ArrayList<double>.))) {
    //      res = ad_deserialization ((val as ArrayList<double>).to_array());          message(res);
    //  }  
    
	return res;
}

string variant_to_string (Variant v){
    string res = "";
    //message(v.get_type().peek_string());
    //message(v.get_type_string());
    
    if (v.get_type().is_basic()){//message("basic");
        if (v.get_type_string() == "i") {res = @"$((int)v)";}    else
        if (v.get_type_string() == "d") {res = @"$((double)v)";} else
        if (v.get_type_string() == "b") {res = @"$((bool)v)";}   else
        if (v.get_type_string() == "s") {res = (string)v;} 
    }
    if (v.get_type().is_array()) {//message("array");
        if (v.get_type_string() == "ai") {res = ai_deserialization((int[])v);}    else
        if (v.get_type_string() == "as") {res = as_deserialization((string[])v);} else
        if (v.get_type_string() == "ab") {res = ab_deserialization((bool[])v);}   else
        if (v.get_type_string() == "ad") {res = ad_deserialization((double[])v);} 
    }

	return res;
}

string bracing(string s){ return "{" + s + "}"; }
string to_json_string(Value obj){
    return Json.gobject_to_data(obj.get_object(),null);
    //message(json_data);
}
//TODO: Temp solution until https://gitlab.gnome.org/GNOME/vala/issues/834
string ai_deserialization(int[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
string ab_deserialization(bool[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");                                         //message(strBuilder.str);
    return bracing(strBuilder.str);
}
string ad_deserialization(double[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");                                         //message(strBuilder.str);
    return bracing(strBuilder.str);
}
string as_deserialization(string[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) {
        strBuilder.append(arr[i]); 
        strBuilder.append(", ");
    }
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
//TODO: Future solution
//  void arr_print<G> (G[] sa){
//      //foreach (var a in sa){ prin(a);}
//  }

void tests_add_funcs(){
    Test.add_func("/better_print/basic_type/int",()    => {assert (variant_to_string(5) == "5");});
    Test.add_func("/better_print/basic_type/bool",()   => {assert (variant_to_string(true) == "true");});
    Test.add_func("/better_print/basic_type/string",() => {assert (variant_to_string("sas") == "sas");});
    Test.add_func("/better_print/basic_type/double",() => {assert (variant_to_string(5.5) == "5.5");});

    Test.add_func("/better_print/array/int",()    => { int[] a={1,2,3};                  assert (variant_to_string(a) == "{1, 2, 3}");});
    Test.add_func("/better_print/array/bool",()   => { bool[] a={true, false, true};     assert (variant_to_string(a) == "{true, false, true}");});
    Test.add_func("/better_print/array/string",() => { string[] a={"sas", "sas", "sas"}; assert (variant_to_string(a) == "{sas, sas, sas}");});
    Test.add_func("/better_print/array/double",() => { double[] a={1.2, 1.3, 1.5};       assert (variant_to_string(a) == "{1.2, 1.3, 1.5}");});

    Test.add_func("/better_print/Gee/int",()    => {var a = new ArrayList<int>.wrap({1,2,3});                  assert (value_to_string(a) == "{1, 2, 3}");});
    Test.add_func("/better_print/Gee/bool",()   => {var a = new ArrayList<bool>.wrap({true, false, true});     assert (value_to_string(a) == "{true, false, true}");});
    Test.add_func("/better_print/Gee/string",() => {var a = new ArrayList<string>.wrap({"sas", "sas", "sas"}); assert (value_to_string(a) == "{sas, sas, sas}");});
    //  Test.add_func("/better_print/Gee/double",() => {var a = new ArrayList<double?>.wrap({1.2, 1.3, 1.5});      assert (basic_to_string(a) == "{1.2, 1.3, 1.5}");});
}

public class MyObject : Object {
    public string str { get; set; }
    public int    num { get; set; }
    //public ArrayList arr { get; set; }

    public MyObject (string str, int num) {
        this.str = str;
        this.num = num;
        //this.arr = arr;
    }
}
void main(string[] args) {
    Test.init(ref args);
    tests_add_funcs();
    Test.run();
    prin(5);
    prin(5.25);
    prin("sas");
    prin(true);


    int[]    a = {1,2,3};
    bool[]   b = {true,false,true};
    double[] c = {1.2,1.3,1.5};
    string[] d = {"sas","sas","sas"};
    prin(a);
    prin(b);
    prin(c);
    prin(d);

    var arrayList = new ArrayList<int>.wrap(a);
    var hashSet   = new HashSet<int>(); hashSet.add_all_array(a);

    prinobj(arrayList);
    prinobj(hashSet);
    prinobj(new MyObject("sas",564));
    

}
