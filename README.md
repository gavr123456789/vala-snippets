# Vala-Snippets


## Kotlin "of" API
```vala
// Gee.HashMap<int,string>
var map = mapOf<int,string>(to(1,"2"),
                            to(3,"4"),
                            to(5,"6"),
                            to(7,"8"));

var list = listOf<int>(1, 2, 3); // Gee.ArrayList<int>
var hashset = setOf<int>(1, 2, 2);// Gee.HashSet<int>
```
## Files
`rename_dirs(".", {"1","2","3"});` // will rename first 3 dir in path/ 

Right now, vala doesn't have a native way to iterate over files or directories recursively, so I did it.
### Examples
```vala
    foreach (var file in get_files(path)) 
        print(file.get_name() + "\n");
    foreach (var file in get_dirs()) // path is current directory by default
        print(file.get_name() + "\n");
```  
I took this feature from Python, foreach on file opens InputStream and returns file line by line.
```vala
    var file_lines = new ForeachFile(path);
    foreach (var line in file_lines) 
        stdout.printf (line + "\n");
```
## Better Print 
It can print via 2 functions: prinobj for objects, and prin
### Examples
prinobj requires json-glib
```vala
    prin(5);     // int
    prin(5.25);  // double
    prin("sas"); // text
    prin(true);  // bool

//  any basic type array
    int[]    a = {1,2,3};
    bool[]   b = {true,false,true};
    double[] c = {1.2,1.3,1.5};
    string[] d = {"sas","sas","sas"};
    
    prin(a); // {1, 2, 3}
    prin(b); // {true, false, true}
    prin(c); // {1.2, 1.3, 1.5}
    prin(d); // {sas, sas, sas}

    
//  any basic type Gee Collection
    var arrayList = new ArrayList<int>.wrap(a);
    var hashSet   = new HashSet<int>(); hashSet.add_all_array(a);

    prinobj(arrayList); // {1, 2, 3}
    prinobj(hashSet);   // {1, 2, 3}
    prinobj(new MyObject("sas",564)); 
    /*
    {
        "str" : "sas",
        "num" : 564
    }
    */
```

## Easy ini-files (WIP)
```vala
var sett = new Setting();
sett.open(); // default filename = settings.conf
sett["key"] = "value"; // ye, just like hashmap 
var name = sett["name"]; // get value
sett.save(); // save to file
```

## ListOf/MapOf API (WIP)  
[Kotlin](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.collections/list-of.html), [Java](https://docs.oracle.com/javase/9/docs/api/java/util/List.html#of-E...-%23of-E...-)  ListOf API
### Examples
Separator can be: space, comma, comma and space. Any type of brackets as desired.
```vala
// ArrayLists
    ls("1 22 33 44 5")        // Gee ArrayList<string>
    li("{1 22 33 44 5}")      // Gee ArrayList<int>
    lb("[false, true, true]") // Gee ArrayList<bool>
    ld(".1 .2 .33")           // Gee ArrayList<double>
// HashMaps
    msi("a:1,b:2")   // Gee HashMap string to int
    mss("a:aa,b:bb") // Gee HashMap string to string
// of func from this [thread](https://mail.gnome.org/archives/vala-list/2019-August/msg00002.html)
    foreach (var item in of("1..10")) 
        print(@"$item, "); // 1, 2, 3, 4, ... 10, 
  }
```

## Fast Examples
Generic/typeWrapper  
SEQ  
Open file with associated program  
Thread  
## OOP  
`as` key word  
basic example  

# WIP
- [ ] Really list of API
- [ ] Add the possibility to output structures structs
- [ ] Add the possibility to output structures classes
