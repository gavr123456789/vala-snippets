void prin(Variant str){ stdout.printf(basic_to_string2(str) + "\n");}

string basic_to_string (Value val){
    //  var typeName = data.type_name(); //message(typeName);
    string res = "";
    
	if (val.type().is_a (typeof (int)))    {res = @"$((int)val)";}    else
    if (val.type().is_a (typeof (string))) {res = (string)val;}       else
    if (val.type().is_a (typeof (bool)))   {res = @"$((bool)val)";}   else
    if (val.type().is_a (typeof (double))) {res = @"$((double)val)";} else
    if (val.type().is_a (typeof (int[]))) {
        int[] arr = (int[])val;
        res += "{";
        foreach (var a in arr) {res += @"$a, ";}
        res += "}";
    }
 
	return res;
}

string basic_to_string2 (Variant v){
    //  var typeName = data.type_name(); //message(typeName);
    string res = "";
    //message(v.get_type().peek_string());
    message(v.get_type_string());
    
    if (v.get_type().is_basic()){message("basic");
        if (v.get_type_string() == "i") {res = @"$((int)v)";} else
        if (v.get_type_string() == "d") {res = @"$((double)v)";} else
        if (v.get_type_string() == "b") {res = @"$((bool)v)";} else
        if (v.get_type_string() == "s") {res = (string)v;} 
    }
    if (v.get_type().is_array()) {message("array");
        if (v.get_type_string() == "ai") {res = ai_deserialization((int[])v);} else
        if (v.get_type_string() == "ab") {res = ab_deserialization((bool[])v);} else
        if (v.get_type_string() == "ad") {res = ad_deserialization((double[])v);} else
        if (v.get_type_string() == "as") {res = as_deserialization((string[])v);} 
    }
    
 
	return res;
}

string bracing(string s){ return "{" + s + "}"; }

string ai_deserialization(int[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
string ab_deserialization(bool[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
string ad_deserialization(double[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) strBuilder.append(@"$(arr[i]), ");
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}
string as_deserialization(string[] arr){
    var strBuilder = new StringBuilder();
    for(var i = 0; i < arr.length - 1; i++) {
        strBuilder.append(arr[i]); 
        strBuilder.append(", ");
    }
    strBuilder.append(@"$(arr[arr.length-1])");
    return bracing(strBuilder.str);
}

void arr_print<G> (G[] sa){
    //foreach (var a in sa){ prin(a);}
}


void main() {
    prin(5);
    prin(5.25);
    prin("sas");
    prin(true);

    int[]a = {1,2,3};
    bool[]b = {true,false,true};
    double[]c = {1.2,1.3,1.5};
    string[]d = {"sas","sas","sas"};
    prin(a);
    prin(b);
    prin(c);
    prin(d);

}