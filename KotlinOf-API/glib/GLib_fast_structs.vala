namespace fast_structs_glib { 

    void parsing_brackets (ref string str){
        if(str.contains("[") || str.contains("(") || str.contains("{")){
            str._delimit("{}[]()",'\\');
            str = str.replace("\\","");
        }
    }
    
    void parsing(ref string str, ref string[] arr){
        parsing_brackets(ref str);

        if(str.contains(",") && !str.contains(" "))
            arr = str.split(",");
        else if (str.contains(" ") && !str.contains(","))
            arr = str.split(" ");
        else if (str.contains(" ") && str.contains(",")){
            str = str.replace(" ","");
            arr = str.split(",");
        }
    }

    void parsing_string(ref string str, ref string[] arr){
        parsing_brackets(ref str);

        if(str.contains(","))
            arr = str.split(",");
        else if (str.contains(" ") && !str.contains(","))
            arr = str.split(" ");
    }

    List<int> gli(owned string str){
        var list = new List<int>();
        string[] arr = {};
        parsing(ref str, ref arr);
        foreach (var item in arr) 
            list.append(int.parse(item));
        return list;
    }

    List<string> gls(owned string str){
        var list = new List<string>();
        string[] arr = {};
        parsing_string(ref str, ref arr);
        //adding
        foreach (var item in arr) 
            list.append(item);
        return list;
    }

    List<bool> glb(owned string str){
        var list = new List<bool>();
        string[] arr = {};
        parsing(ref str, ref arr);
        foreach (var item in arr) 
            list.append(bool.parse(item));
        return list;
    }

}