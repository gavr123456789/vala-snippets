using fast_structs_gee;
//Compile valac gee_lib_test.vala  Gee_fast_structs.vala --pkg gee-0.8

void main(){
  //string arraylist test
  ls("1 22 33 44 5").@foreach( (a) => {print(@"$a\t"); return true;} ); print("\n");
  //int arraylist test
  li("{1 22 33 44 5}").@foreach( (a) => {a++; print(@"$a\t"); return true;} ); print("\n");
  //bool arraylist test
  lb("[false, true, true]").@foreach( (a) => {a = true; print(@"$a\t"); return true;} ); print("\n");
  
  //double arraylist test
  ld(".1 .2 .33").@foreach( (a) => {print(@"$a\t"); return true;} ); print("\n");
  
  //map string to int test
  foreach (var i in msi("a:1,b:2").entries) 
    print(@"$(i.key):$(i.value)\t");                print("\n");
  //map string to string test
  foreach (var i in mss("a:aa,b:bb").entries) 
    print(@"$(i.key):$(i.value)\t");                print("\n");

  foreach (var item in of("1..10")) {
    print(@"$item, ");
  }
  
}
