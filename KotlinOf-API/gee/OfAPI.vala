using Gee;

[Compact] class Map<K,V>{
    public Map(K k, V v){
        this.k = k;
        this.v = v;
    }
    public K k;
    public V v;
}

Map<K,V> to<K,V>(K k, V v){
    return new Map<K,V>(k, v);
}

HashMap<K,V> mapOf<K,V>(params Map<K,V>?[] maps){
    var map = new HashMap<K,V>();
    foreach (var onemap in maps)
            map[onemap.k] = onemap.v;
    
    return map;
}

ArrayList<T> listOf<T>(params T[] t){
    var list = new ArrayList<T>();
    foreach (var a in t)
        list.add(a);
    
    return list;
}

Set<T> setOf<T>(params T[] t){
    var list = new HashSet<T>();
    foreach (var a in t)
        list.add(a);
    
    return list;
}

void main(){
    var map = mapOf<int,string>(to(1,"2"),
                                to(3,"4"),
                                to(5,"6"),
                                to(7,"8"));

    var list = listOf<int>(1, 2, 3);
    var hashset = setOf<int>(1, 2, 2);

    foreach (var a in map.entries)
        prin(a.key, " = ",a.value);

    prin();
    
    foreach (var a in list)
        print(@"$a, ");

    prin();

    foreach (var a in hashset)
        print(@"$a, ");
}


[Print] void prin(string s){stdout.printf(s + "\n");}