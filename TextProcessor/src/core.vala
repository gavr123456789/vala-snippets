using Gpseq;

delegate string TextFunc (string text);
delegate bool CheckFunc (string text);
// Vala cant create delegate array, the ony way is wrap them or use ccode has_target = false
// Check this https://bugzilla.gnome.org/show_bug.cgi?id=526549
class WrapProcessDelegate{
    public TextFunc textFunc;
    public WrapProcessDelegate(TextFunc func){ textFunc = func;}
}
class WrapCheckDelegate{
    public CheckFunc checkFunc;
    public WrapCheckDelegate(CheckFunc func){ checkFunc = func;} 
}

struct Package {
    WrapProcessDelegate[] process_funcs;
    WrapCheckDelegate[] check_funcs;
    bool is_parallel;
    public Package(WrapProcessDelegate[] procs, WrapCheckDelegate[] checks, bool parallel = false){
        process_funcs = procs;
        check_funcs = checks;
        is_parallel = parallel;
    }
}

class Core : Object {
    public TextStorage textStorage;
    public Package?[] package = {};
    public Core (string text) {
        textStorage = new TextStorage(text);
    }
    
    public inline void add_package( Package pac){
        package += pac;
    }

    //  public void line_process() requires(package[0].check_funcs.length!=0) {
    //      for (int i = 0; i < textStorage.lines.length; i++)
    //          foreach (var pac in package){
    //              bool flag = true;//флаг отвечающий за то все ли функции чекеры прошли
    //              foreach (var check_func in pac.check_funcs){
    //                  if (!check_func.checkFunc(textStorage.lines[i])){
    //                      flag = false; // если хотя бы одно условие не прошло то флаг зафейлен
    //                  }
    //                  //  message(@"$(textStorage.lines[i]) = $(!check_func.checkFunc(textStorage.lines[i]))\n");
    //              }
    //              if (flag){
    //                  foreach (var proc_func in pac.process_funcs ){
    //                      textStorage.lines[i] = proc_func.textFunc(textStorage.lines[i]);
    //                  }
    //              }
    //          }
    //      save_to_result();
    //  }
    // need first iterate packages, then lines foreach package
    // if there will be iterate throw 2 items, there must be checks that this element not lastest
    public void line_process_seq()throws Error requires(package[0].process_funcs.length!=0)  {
        bool flag = true;
        //  Seq.iterate<int>(0, i => i < textStorage.lines.length, i => ++i).parallel()
        Seq.of_array(package)//TODO check what fo_owned_array doing
        .foreach(pac => {
            var seq_of_lines = (pac.is_parallel)?Seq.iterate<int>(0, i => i < textStorage.lines.length, i => ++i).parallel():
                                                 Seq.iterate<int>(0, i => i < textStorage.lines.length, i => ++i);
            seq_of_lines.foreach(line_num => {
                
                flag = true;
                //checks always parralel
                var seq_of_checks =    /*(pac.is_parallel)?*/ Seq.of_array(pac.check_funcs).parallel()/*:Seq.of_array(pac.check_funcs)*/;
                var seq_of_processes = (pac.is_parallel)? Seq.of_array(pac.process_funcs).parallel():Seq.of_array(pac.process_funcs);

                seq_of_checks.foreach(check => {
                    if (!check.checkFunc(textStorage.lines[line_num]))
                        flag = false;
                }).wait();
    
                if (flag)//all checks was passed
                    seq_of_processes.foreach(process =>{
                        textStorage.lines[line_num] = process.textFunc(textStorage.lines[line_num]);
                }).wait();
              

            }).wait();
        });

        foreach (unowned string s in textStorage.lines) prin("sas ", s);
        save_to_result();
    }
    void save_to_result(){
        var b = new StringBuilder();
        
        foreach (unowned string s in textStorage.lines){
            b.append(s);
            b.append("\n");
        }
        textStorage.result = (owned)b.str;
    }
}