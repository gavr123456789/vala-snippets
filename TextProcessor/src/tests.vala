using Gpseq;
void main(){
    var text = 
"a
c
c";
    prin("before: \n", text);
    var core = new Core(text);
    var proc_func = new WrapProcessDelegate(a => {return "b";});
    var check_func = new WrapCheckDelegate(a => {return (a.chug() == "a")?true:false;});

    core.add_package(Package({proc_func},{check_func},true));
    core.line_process_seq();
    prin("\nafter: \n",core.textStorage.result);

    
}


[Print] void prin(string s){stdout.printf(s + "\n");}